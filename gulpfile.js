var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var $ = require('gulp-load-plugins')();
var autoprefixer = require('autoprefixer');

var sassPaths = [
  'node_modules/foundation-sites/scss',
  'node_modules/motion-ui/src'
];

function sass() {
  return gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.postcss([
      autoprefixer({ browsers: ['last 2 versions', 'ie >= 9'] })
    ]))
    .pipe(gulp.dest('public/css'))
};


function watch(){
  
  // Watch styles
  gulp.watch("scss/*.scss", sass);
  
  // Watch webwork
  nodemon({
    script: '_app.js',
    ext: 'js pug',
    env: { 'NODE_ENV': 'development' },
  });
}

gulp.task('sass', sass);
gulp.task('watch', gulp.series('sass', watch));
gulp.task('default', gulp.series('sass', watch));


//======{  TRASH HEAP. THANKS A LOT, ZURB. }======//

// Removed dependency
// var browserSync   = require('browser-sync').create();

// Last line in 'sass' function 
// .pipe(browserSync.stream());

// Removed default tasks
// gulp.task('serve', gulp.series('sass', serve));
// gulp.task('default', gulp.series('sass', serve));

/* Browsersync (good for static files, fucking blows for apps)
function serve() {
  browserSync.init({
    server: "./"
  });

  gulp.watch("scss/*.scss", sass);
  gulp.watch("*.html").on('change', browserSync.reload);
}
*/