
$(document).foundation();
$(function(){
	
	
	if(scrollSpeed === undefined){
		var scrollSpeed = 750;
	}
	$('.scroll').click(function(evt){	// Smooth scrollin'
		evt.preventDefault();
		var gotoId = $(this).attr('href');
		$('html, body').animate({
			scrollTop: ($(gotoId).offset().top - $('#nav').outerHeight())
		}, scrollSpeed);
	});
	
	//==============={  NAV BEHAVIOR  }===============//
	var didScroll = false;
	var lastScrollTop = 0;
	var delta = 5;
	var navPersists = 750;
	// Stuff for white nav mgmt
	var navScrolled = false;
	var navDistance = 40;	// How far down does the user scroll before '.scrolled' is added?
	
	$(window).scroll(function(){
		var height = $(window).scrollTop();
		didScroll = true;
		hasScrolled(height);
		runNavState(height);
		
		var count = 1;	// Debug elements to fade on page
		console.log("Elements being faded in:");
		$.each($(".fade-me"), function(){
			console.log(count);
			count++;
			if($(this).is(':in-viewport')){
				$(this).addClass('faded-in');
			}else{
				
			}
		});
		
	});
	
	function runNavState(height){
		if(height <= navDistance){
			$("#nav").removeClass('scrolled');
			navScrolled = false;
			// console.log("Nav transitioned OUT OF scrolled state");
		}
		if(height >= navDistance && navScrolled == false){
			$("#nav").addClass('scrolled');
			// $("#navLogo").removeClass('initial');
			navScrolled = true;
			// console.log("Nav transitioned to scrolled state");
		}
	}
	
	function hasScrolled(height){	// Detect scroll direction
		var top = height;
		if (Math.abs(lastScrollTop - top) <= delta) {
			return;
		}
		if (top > lastScrollTop && top > delta && top > navPersists) { // Scrolling Down
			$("#nav").addClass('up');
		} else { // Scrolling up
			$("#nav").removeClass('up');
		}
		lastScrollTop = top;
	}
	
	// Toggle nav menu
	$('#nav-button').click(function () {
		if (!$(this).hasClass('open')) {
			$('#nav-right, #nav-button, #hamburger').removeClass('closed').addClass('open');
		} else {
			$('#nav-right, #nav-button, #hamburger').removeClass('open').addClass('closed');
		}
	});
});
	